# Correction de finaux SR01

Ne pas hésiter à contribuer en me demandant de vous ajouter au projet Gitlab. <sylvain.marchienne@etu.utc.fr>


# Fiche mémos

## Programmation système

Un processus peut se terminer normalement avec OU sans erreurs (`exit/return EXIT_SUCCESS / EXIT_FAILURE`).
Un processus peut se terminer anormalement (à cause d’un signal NON INTERCEPTÉ, par exemple SIGKILL, ou tout autre signal qui provoquerait la terminaison du programme sans qu’il soit intercepté et traité par un handler qui en ferait un `exit()`)

`int atexit( void (*function) (void) )`
* lorsque le processus se termine normalement, la function en argument est exécutée (doit être void avec 0 argument)
* normalement = pas terminé avec un signal, terminé avec un exit/return (même si code d’erreur)
* donc si exit/return => atexit est déclenché même si c’est un numéro d’erreur qui est retourné/exité
* on peut donner plusieurs fonctions à appeler:
 * atexit(func1);
 * atexit(func2);
 * elles sont appelées dans l’ordre inverse (LIFO):
  * func2 puis func1

#### Fork & pid

* `pid_t getpid()`
 * retourne le numéro de processus courant


* `char* getenv(var)`
 * retourne la valeur associée à la variable d’environnement var


* `void perror(msg)`
 * écrit sur la sortie d'erreur standard le message msg
 * msg précédera le message standard associé à la dernière erreur rencontrée, stockée dans errno
 * le numéro de la dernière erreur rencontrée est stocké dans la variable errno définie et utilisable avec `#include <errno.h>`


* `pid_t fork()`
 * retourne un numéro de type pid_t
   * 0 si on est dans le processus fils
   * \> 0 si on est dans le processus père, dans ce cas le numéro est le pid du processus fils tout juste créé
   * -1 si erreur lors du fork (pas assez de mémoire pour fork)


* `wait(&status)`
 * suspend l'exécution du processus appelant jusqu'à ce que l'un de ses fils se termine
 * équivalent à waitpid(-1, &status, 0);
 * retourne le pid du fils qui s’est terminé


* `waitpid(pid, &status, 0)`
 * pid est le pid du processus fils à attendre
 * pid = -1 pour n’importe quel processus fils
 * pid > 0 pour attendre un processus fils de pid égal à pid
 * &status pour stocker dans status le nouveau statut du pid attendu
 * 0 pour pas d’options additionnelles
retourne également le pid du fils qui s’est terminé


* Interprétation du status:
 * `WIFEXITED(status)`
   * Elle renvoie vrai si le statut provient d'un processus fils qui s'est terminé normalement (cad exit() ou return du main)
 * `WEXITSTATUS(status)`
   * Elle renvoie le code de retour du processus fils passé à exit/return (EXIT_SUCCESS = 0, EXIT_FAILURE = 1, etc)
utilisable uniquement si on a check WIFEXITED avant à vrai
 * `WIFSIGNALED(status)`
   * Elle renvoie vrai si le statut provient d'un processus fils qui s'est terminé à cause d'un signal (terminé anormalement donc)
 * `WTERMSIG(status)`
   * Elle renvoie la valeur du signal qui a provoqué la terminaison du processus fils.
utilisable uniquement si on a check WIFSIGNALED avant à vrai


* Zombies
 * ce sont des processus fils terminés dont le status n’a pas été lu avec un wait() par le processus père
 * si le processus père meurt sans avoir wait tous les fils, les fils seront définitivement effacés avec le processus init de PID 1 quand même
 * des processus fils sont zombies quand ils sont terminés et que le père ne les a pas wait et qu’il n’est pas lui même terminé


#### Famille exec & system

Les fonctions de la famille exec exécutent un programme (pas une commande shell, un programme, d’où le chemin vers /bin/ls dans les examples ci dessous). Ils écrasent le processus qui les a appelé (contrairement à system).

* `execl(path, arg0, arg1, …, (char*)0)`
 * les arguments de la commande cmd à exécuter sont données sous forme de liste (suffixe -l) terminée par NULL cad (char*)0
 * ex: `execl(‘/bin/ls’, ‘ls’, ‘-l’, (char*)0)`
 * `/bin/ls` est le chemin du programme à exécuter
 * `ls` est l’arg[0] passé à ce programme
 * execlp est identique, à la différence que le fichier à exécuter en argument pourra être cherché dans les répertoires listés dans la variable d’environnement `PATH` du shell


* `execv(path, [arg0, arg1, …, (char*)0])`
 * les arguments sont données dans un tableau terminé par NULL
 * ex: `execv(‘/bin/ls’, [‘ls’, ‘-l’, (char*)0])`
 * execvp meme raisonnement que execlp


* `system(cmd)`
 * exécute une commande shell sans tuer le processus courant

#### Signaux

https://en.wikipedia.org/wiki/C_signal_handling

* `int kill(pid_t pid, int sig)`
 * envoi un signal au processus pid avec le signal sig
 * équivalent à `kill -sig pid` dans le shell
 * SIGKILL est le numéro de signal indiquant la terminaison immédiate du processus
 * `kill(pid, SIGKILL)` terminera le processus dont le pid est pid
 * SIGKILL est équivalent à 9 (`kill -9 pid`)
 * SIGINT demande au programme de se terminer, mais celui-ci peut ignorer (contrairement à SIGKILL)


* Structure `Sigaction` pour définir le comportement à la réception d’un signal:
 * `void func(int n) { … };`
 * `struct sigaction S;`
 * `S.handler = func;`
 * `sigaction(SIGNAL, &S, NULL);`

Ou utilise la fonction `signal()` (deprecated) mais plus simple
`signal(SIGNAL, func)`;

Les signaux `SIGKILL` et `SIGSTOP` ne peuvent pas être associés à une fonction, le système refuse.
>  « A signal handler can be specified for all but two signals (SIGKILL and SIGSTOP cannot be caught, blocked or ignored). »

Mais c’est possible avec SIGINT, qui correspond au `ctrl+C` que l’utilisateur peut faire en console. Ce signal peut donc être ignoré (si on ne fait rien dans la fonction handler pour terminer le programme).

#### Descripteurs

* Descripteur: numéro, pointeur vers le flux
un fichier reste ouvert tant qu’il existe un descripteur non "closé"


* Table des descripteurs de base:
 * 0: `stdin`
 * 1: `stdout`
 * 2: `stderr` = `stdout` aussi mais pour les erreurs


* `printf()` affiche automatiquement sur le descripteur de rang 1.
* `scanf` lit automatiquement sur le descripteur de rang 0.
* Si on `close(1)` juste avant un `printf`, alors ce `printf` n’affiche rien.


* Pipe:
 * permet de créer un tube pour envoyer des messages entre un père et un fils.
 * pipe(fd) doit être appelé avant le fork(). fd doit être un tableau de taille 2.
 * La première valeur correspond, une fois le pipe appelé sur fd, à un descripteur de sortie (fd[0] est la sortie, cad côté sur lequel on lit).
 * La deuxième valeur correspond à un descripteur de lecture (fd[1] est l’entrée, côté sur lequel on écrit).

```C
int fd[2];
int n;
pipe(fd);
pid = fork();

if(pid == (pid_t) 0) {
  close(fd[0]); // le fils ne lit pas
  write(fd[1], 45, sizeof(int));
} else if(pid > (pid_t)0) {
  close(fd[1]); // le père n’écrit pas
  read(fd[0], &n, sizeof(int);
}
```

* `int fd = dup(fd)`
 * duplique le descripteur dans la table des descripteur, dont le rang est fd (rang = indice dans la table)
 * insère la copie au 1er index de la table qui est vide
retourne le rang du descripteur ajouté dans la table


* `ind fd = dup2(int source, int destination)`
 * recopie du descripteur source dans l'entrée destination de la table des descripteurs
 * source est le descripteur qui va remplacer le descripteur destination
 * `dup2(fd[0], 0);`
   * remplace stdin (0) par un pipe côté lecture (fd[0])
   * `scanf(""%d", &var);` lit dans le pipe à la place de stdin
 * `dup2(fd[1], 1);`
   * remplace stdout (1) par un pipe côté écriture (fd[1])
   * `printf("%d", var);` écrit dans le pipe à la place de stdout
 * `printf` écrit toujours dans le descripteur de rang 1
 * `scanf` lit toujours dans le descripteur de rang 0


* `close(fd)`
 * supprime le descripteur de la table dont le rang est fd (rang = indice dans la table)


* `int fd = open(fichier, options, mode)`
 * ouvre un fichier et retourne le rang du descripteur ajouté dans la table associé à ce fichier
 * penser à faire un close(fd) pour supprimer ce descripteur à la fin
 * ex: `open("toto.img", 0, 0666);`


* `read(fd, &cible, sizeof(type))`
 * lit sur le descripteur fd[0] (pipe, le 0 est la sortie / point de lecture)
 * stock la lecture dans la variable cible (on doit passer son pointeur)
 * on indique la taille en bytes de ce qu’on lit, sizeof(int)
 * ex: `int tableau[10];`
 * ex: `read(fd, tableau, 10 * sizeof(int))`


* `write(fd[1], &source, sizeof(type))`
 * écrit sur le descripteur fd[1] (pipe, le 1 est l’entrée / point d’écriture)
 * puise les données dans la variable source
 * on indique la taille des données sources
 * ex: `int tableau[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};`
 * ex: `write(fd[1], tableau, 10 * sizeof(int))`


## Regex

* `grep -E` pour accepter les regex de la forme `^ … $`
 * `cat mails.txt | grep -E ^[[:alpha:]\.\_]+@[[:alpha:]\.\_]+\.[a-z]+$`


* `grep -v` pour inverser: cad sortir tout ce qui n’est pas la regex passée
 * `cat mails.txt | grep -vE regexp`


* `grep -c` pour compter le nombre de lignes/résultats:
 * ex: `cat mails.txt | grep -Ec regexp` le nombre de lignes trouvés
 * ex: `cat mails.txt | grep -vcE regexp` le nombre de résultats non trouvés (-v)


* Numéros de téléphone
 * https://regex101.com/r/0B2wpQ/1
* URLs
 * https://regex101.com/r/0B2wpQ/2


## Compilation & makefile

* Compiler un projet (main.c, file.c, file.h) directement en exécutable:
 * `gcc -c file.c -> file.o` (les .h n’ont pas besoin d’être spécifiés)
 * `gcc -c main.c -> main.o`
 * `gcc -o prg main.o file.o -lnom_librairie`


* Ou plus directement:
 * `gcc -o prg main.c file.c -lnom_librairie`


* Convertir un fichier objet en libraire:
 * `ar rvs nom_librairie.a file.o`


* Lancer le makefile en console:
 * make [règle]
   * ex: `make clean`
 * make
   * lance la 1ère règle du makefile
 * `.PHONY: cible1`
   * pour indiquer quelles règles ne sont pas des fichiers sources

* Les règles sont toujours de la forme:
```makefile
cible: dépendances
  action
```

Il faut mettre dans les dépendances tous les fichier .h qui sont `#include` dans la cible. En revanche, il ne faut pas les mettre dans `gcc`.
