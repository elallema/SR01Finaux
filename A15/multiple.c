#include <stdlib.h>

int main(int argc, char *argv[]) {

  int a,b,i;
  int fd[2];
  pid_t pids[256];

  if(argc == 1) {
    printf("Il fut fournir un argument !\n");
    exit(1);
  }

  if(getenv("NUMBER_WORKERS") != '\0')
    int a = atoi(getenv("NUMBER_WORKERS"));
  else {
    printf("La variable d'environnement NUMBER_WORKERS n'existe pas.");
    exit(1);
  }

  if(getenv("DIFFICULTY") != '\0')
    int b = atoi(getenv("DIFFICULTY"));
  else {
    printf("La variable d'environnement DIFFICULTY n'existe pas.");
    exit(1);
  }

  printf("HEADER = %s\n", argv[1]);
  printf("NUMBER_WORKERS = %d\n", a);
  printf("DIFFICULTY = %d\n", b);

  pipe(fd);

  for(i = 0; i < a; i++) {
    pids[i] = fork();
    if(pids[i] == (pid_t)0) {
      // processus fils,
      close(fd[0]); // car les fils de lisent jamais dans le pipe
      startWorker(argv[1], getenv("DIFFICULTY"), fd[1]);
      // normalement le processus fils est terminé dans startWorker
      // si on exit(2) ici, c'est qu'il y a eu un problème
      exit(2);
    }
  }
  
  close(fd[1]); // le père n'écrit pas
  startMiningManager(pids, a, fd[0]);
  exit(0);
}

void startWorker(char header[], char difficulty[], int writeFD) {

  close(1); // on ferme le STDOUT_FILENO
  dup(writeFD);  // on le remplace par l'entrée de notre pipe

  // OU plus simple: dup2(writeFD, 1);
  // cette commande remplace 1 (STDOUT_FILENO) par writeFD
  // dans la table des descripteurs

  // On exécute le programme mineur, avec comme arguments:
  // 1er: header
  // 2ème: difficulty
  // on termine la liste des args avec un pointeur NULL
  execlp("mineur", header, difficulty, (char*)0);

}

void startMiningManager(pid_t pids[], int size, int readFD) {
  int i, nonce;

  // on attend une donnée de taille sizeof(int) par le descripteur readFD,
  // que l'on stocke dans la variable nonce
  read(readFD, &nonce, sizeof(int));


  // on SIGKILL chaque processus fils
  for(i = 0; i < size, i++)
    kill(pids[i], SIGKILL);

  // on attend leur mort pour les sortir de la zone des zombies
  // car un processus est un zombie lorsqu'il est terminé mais que son
  // père ne l'a pas attendu avec waitpid
  int status;
  for(i = 0; i < size, i++)
    waitpid(pids[i], &status, 0);
}

Expression régulière:
^[0-9a-f]{80}$
