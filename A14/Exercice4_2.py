from tkinter import *

def code_ascii(event):
    chaine_saisie = entree.get()
    if (not chaine_saisie) : # la chaine est vide
        return 0
    label.configure(text = "ASCII de est: " + str(ord(chaine_saisie)))
    entree.delete(0) # vider le champ de texte

N=0
fenetre = Tk() #1
entree=Entry(fenetre,width=1,bd=4)
entree.pack(side=TOP) #2
entree.bind("<KeyRelease>", code_ascii) #3
label=Label(fenetre, text= 'ASCII de est: ')
label.pack(side=BOTTOM) #4
fenetre.mainloop() #5
