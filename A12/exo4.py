def intersect(l1, l2):
    l = []
    for x in l1:
        if x in l2 and not x in l:
            l.append(x)
    return l

# print(intersect([1,2,3], [3,5,1]))

def maxi(l1):
    max = l1[0]
    for x in l1:
        if x > max:
            max = x
    return max

# print(maxi([1, 2, 4, 3]))

# Tri par sélection
def trier(l):
    for i in range(len(l)-1, 0, -1):
        m = l.index(maxi(l[0:i]))
        if l[m] > l[i]:
            x = l[i]
            l[i] = l[m]
            l[m] = x
    return l

#print(trier([2, 3, 6, 1, 4, 4, 10]))
