int main(int argc, char* argv[]) {

  int status;
  pid_t fils = fork();

  if(!fils) {
    // on est dans le processus fils
    execvp(argv[1], {argv[1], NULL});

  } else {
    // on est dans le père
    wait(&status);

    if(WIFEXITED(status)) {
      // le fils s'est terminé normalement
      printf("%s s'est terminé normalement avec la valeur %d\n", argv[1], WIFEXITED(status));
    } else if(WIFSIGNALED(status)) {
      // le fils s'est terminé anormalement à cause d'un signal
      printf("%s s'est terminé anormalement à cause du signal %d\n", argv[1], WTERMSIG(status));
    }

  }


}
