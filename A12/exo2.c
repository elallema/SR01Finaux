#include <stdio.h>

void handler_lecteur(int x) {
  int fd = open("toto.img", 0, 0666);
  read(fd, image, size * size * sizeof(char)); // lit le fichier image

  // transpose la matrice image
  image = ...

  pipe(piped); // créé le pipe sur la variable globale piped
  write(piped[1], image, size * size * sizeof(char));  // écrit le résultat dans le pipe
  close(fd);
  close(piped[1]); // ferme le descripteur d'écriture du pipe
  exit(0);  // termine le processus p_lecteur
}

void handler_redacteur(int x) {
  int fd = open("toto.img", 0, 0666);
  read(piped[0], image, size * size * sizeof(char));    // lit le résultat de la transposée dans le pipe
  write(fd, image, size * size * sizeof(char));         // écrit le résultat dans le fichier
  close(fd);
  close(piped[0]); // ferme le descripteur de lecture du pipe
  exit(0);  // termine le processus p_redacteur
}

int main() {

  int status;

  p_lecteur = fork();
  signal(SIGUSR1, handler_redacteur); // on définit le handler de ce processus pour le signal SIGUSR1

  if(!p_lecteur)  { // si p_lecteur == 0, c'est à dire si on est dans le processus p_lecteur

    signal(SIGUSR1, handler_lecteur); // on définit le handler de ce processus pour le signal SIGUSR1
    pause();

  } else {

    p_redacteur = fork();

    if(!p_redacteur) {  // si p_redacteur == 0, c'est à dire si on est dans le processus p_redacteur

      kill(p_lecteur, SIGUSR1); // on déclenche handler_lecteur
      pause();

    } else {

      sleep(2);
      kill(p_redacteur, SIGUSR1); // on déclenche handler_redacteur, p_redacteur ayant hérité de signal(SIGUSR1, handler_redacteur);
      while(wait(&status) != -1);

    }

  }


  return 0;
}
