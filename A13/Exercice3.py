# on suppose qu'on cherche l'intersection sans doublons
def intersection(l1, l2, l3):
    l = []
    for i in range(0, len(l1)):
        for j in range(0, len(l2)):
            if l1[i] == l2[j]:
                for k in range(0, len(l3)):
                    if l2[j] == l3[k]:
                        # On vérifie que la valeur n'existe pas déjà
                        exists = False
                        for h in range(0, len(l)):
                            if l[h] == l3[k]:
                                exists = True
                                break
                        # Si la valeur n'est pas dans la liste on l'ajoute
                        if not exists: l.append(l3[k])
    return l


# Plus simple
def intersection(l1, l2, l3):
    l = []
    for x in l1:
        if x not in l and x in l2 and x in l3:
            l.append(x)
    return l

print(intersection([1, 2, 2], [2, 4, 1], [1, 6, 2]))
