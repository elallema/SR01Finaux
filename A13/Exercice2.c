/* =========================================================
 * Questions 1 & 2
 * ========================================================= */

int maxi(int i, int j) {
  if(i > j) return i;
  else return j;
}

int max(int* tab, int debut, int fin) {
  int i, _max;
  for(i = debut; i <= fin; i++)
    _max = maxi(_max, tab[i]);
  return _max;
}

/* =========================================================
 * Question 3
 * ========================================================= */

 int compute(int* tab, int debut, int fin)
 {
   int pid1, pid2, milieu, status;

   /* Ce bloc d’instructions permet de créer deux noms de fichiers, un
   pour le premier fils et un pour le deuxième fils. Le nom du fichier est
   composé du pid du père et de l’identifiant 1 pour le premier fils, et de
   l’identifiant 2 pour le deuxième fils */
   char name1[256] = "fic1_";
   char name2[256] = "fic2_";
   char buffer[100];
   sprintf(buffer,"%d",getpid());
   strcat(name1,buffer);
   strcat(name1,".txt");
   strcat(name2,buffer);
   strcat(name2,".txt");

   if( (fin-debut) <= seuil)
    return max(tab, debut, fin);

   milieu = debut + (fin-debut)/2;

   pid1 = fork();

   if(pid1 == 0)
   {
     // On est dans le processus fils n°1, on applique la
     // recherche sur la première moitiée
     ecrire_fils(compute(tab, debut, milieu), name1); // on écrit le résultat
     return 0;  // on termine le processus pid1
   }
   else
   {
     pid2 = fork();

     if(pid2 == 0)
     {
       // On est dans le processus fils n°2, on applique la
       // recherche sur la deuxième moitiée
       ecrire_fils(compute(tab, milieu, fin), name2); // on écrit le résultat
       return 0;  // on termine le processus pidé
     }
     else
     {
       // On est dans le processus père,
       // on attend la fin de recherche sur la
       // 1ère et 2ème moitiée faite par les processus
       // pid1 et pid2

       int j1 = 0, j2 = 0;

       // A compléter … … (3)
       // on attend que le pid1 finisse sa recherche
       waitpid(pid1, &status, 0);
       lire_pere(&j1, name1);
       printf("Mon fils 1 a écrit %d \n", j1);

       // A compléter … … (4)
       // on attend que le pid2 finisse sa recherche
       waitpid(pid2, &status, 0);
       lire_pere(&j2, name2);
       printf("Mon fils 2 a écrit %d \n", j2);

       printf("Le maximum de j1 et j2 est: %d\n", maxi(j1, j2));
       return maxi(j1, j2);
     }

   }
 }

 main() {compute(tab, 0,9);}
